return {
	{
		"nvim-treesitter/nvim-treesitter",
		event = { "BufReadPre", "BufNewFile" },
		build = ":TSUpdate",
		dependencies = { "windwp/nvim-ts-autotag" },
		config = function()
			require("nvim-treesitter.configs").setup({
				highlight = {
					enable = true,
					additional_vim_regex_highlighting = false,
				},
				indent = { enable = true },
				autotag = { enable = true },
				auto_install = true,
				sync_install = false,
				ensure_installed = {
					"c",
					"lua",
					"go",
					"templ",
					"kotlin",
					"html",
					"css",
					"bash",
					"javascript",
					"typescript",
					"gitignore",
				},
			})
		end,
	},
}

