return {
  'stevearc/oil.nvim',
  opts = {},
  -- Optional dependencies
  dependencies = { { "echasnovski/mini.icons", opts = {} } },
  -- dependencies = { "nvim-tree/nvim-web-devicons" }, -- use if prefer nvim-web-devicons
  config = function()
	require("oil").setup({
			skip_confirm_for_simple_edits = true,
			view_options = {
				show_hidden = true,
				is_always_hidden = function(name, _)
					local isParent = vim.startswith(name, "..")
					local isTemplGen = vim.endswith(name, "_templ.go")
					return isParent or isTemplGen
				end,
				keymaps = {
					["<BS>"] = "actions.parent",
				},
			},
		})

		local wk = require("which-key")
		wk.add({
			{ "<BS>", ":Oil<CR>", desc = "Open file tree" },
		})
  end,
}
