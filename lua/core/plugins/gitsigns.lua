return {
	"lewis6991/gitsigns.nvim",
	config = function()
		require("gitsigns").setup({
			signs = {
				add = { text = "│" },
				change = { text = "│" },
				delete = { text = "󰍵" },
				topdelete = { text = "‾" },
				changedelete = { text = "~" },
				untracked = { text = "+" },
			},
			signcolumn = true, -- Toggle with `:Gitsigns toggle_signs`
			numhl = false, -- Toggle with `:Gitsigns toggle_numhl`
			linehl = false, -- Toggle with `:Gitsigns toggle_linehl`
			word_diff = false, -- Toggle with `:Gitsigns toggle_word_diff`
			watch_gitdir = {
				follow_files = true,
			},
			attach_to_untracked = true,
			current_line_blame = false, -- Toggle with `:Gitsigns toggle_current_line_blame`
			current_line_blame_opts = {
				virt_text = true,
				virt_text_pos = "eol", -- 'eol' | 'overlay' | 'right_align'
				delay = 1000,
				ignore_whitespace = false,
				virt_text_priority = 100,
			},
			current_line_blame_formatter = "<author>, <author_time:%Y-%m-%d> - <summary>",
			sign_priority = 6,
			update_debounce = 100,
			status_formatter = nil, -- Use default
			max_file_length = 40000, -- Disable if file is longer than this (in lines)
			preview_config = {
				-- Options passed to nvim_open_win
				border = "single",
				style = "minimal",
				relative = "cursor",
				row = 0,
				col = 1,
			},
			on_attach = function(_)
				local wk = require("which-key")
				wk.add({
					{ "[g", ":Gitsigns prev_hunk<CR>", desc = "Go to previous git hunk" },
					{ "]g", ":Gitsigns next_hunk<CR>", desc = "Go to next git hunk" },
					{ "<leader>gah", ":Gitsigns stage_hunk<CR>", desc = "Add hunk to git stage" },
					{ "<leader>gab", ":Gitsigns stage_buffer<CR>", desc = "Add buffer to git stage" },
					{ "<leader>gdh", ":Gitsigns undo_stage_hunk<CR>", desc = "Remove hunk from git stage" },
					{ "<leader>guh", ":Gitsigns reset_hunk<CR>", desc = "Reset git hunk" },
					{ "<leader>gub", ":Gitsigns reset_buffer<CR>", desc = "Reset git buffer" },
					{ "<leader>gk", ":Gitsigns preview_hunk<CR>", desc = "Preview git hunk" },
					{ "<leader>gK", ":Gitsigns diffthis<CR>", desc = "Show git diff" },
				})
			end,
		})
	end,
}

