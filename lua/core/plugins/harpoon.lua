return {
	"ThePrimeagen/harpoon",
	branch = "harpoon2",
	dependencies = { "nvim-lua/plenary.nvim" },
	config = function()
		local harpoon = require("harpoon")
		harpoon:setup()

		local switchToFile = function(num)
			return {
				string.format("<leader>%d", num),
				function()
					harpoon:list():select(num)
				end,
				desc = string.format("Switch to Harpoon file %d", num),
			}
		end

		local wk = require("which-key")
		wk.add({
			{ "<leader>m",    function() harpoon:list():add() end,                         desc = "Add file to Harpoon" },
			{ "<leader>h",    function() harpoon.ui:toggle_quick_menu(harpoon:list()) end, desc = "Toggle Harpoon menu" },
			{ switchToFile(1) },
			{ switchToFile(2) },
			{ switchToFile(3) },
			{ switchToFile(4) },
			{ switchToFile(5) },
		})
	end,
}
