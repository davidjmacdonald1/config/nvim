return {
	"nvim-telescope/telescope.nvim",
	branch = "0.1.x",
	dependencies = {
		"nvim-lua/plenary.nvim",
		"nvim-telescope/telescope-fzf-native.nvim",
		"debugloop/telescope-undo.nvim",
	},
	config = function()
		require("telescope").setup({
			file_ignore_patterns = { "node%_modules/.*" },
			extensions = {
				fzy_native = {
					override_generic_sorter = false,
					override_file_sorter = true,
				},
				undo = {},
			},
		})

		local builtin = require("telescope.builtin")
		local wk = require("which-key")
		wk.add({
			{ "<leader>ff", builtin.find_files,                                     desc = "Search project files" },
			{ "<leader>fa", ":Telescope find_files hidden=true no_ignore=true<CR>", desc = "Search all project files" },
			{
				"<leader>fw",
				function()
					local word = vim.fn.expand("<cword>")
					builtin.grep_string({ search = word })
				end,
				desc = "Search for next word",
			},
			{
				"<leader>fW",
				function()
					local word = vim.fn.expand("<cWORD>")
					builtin.grep_string({ search = word })
				end,
				desc = "Search for previous word",
			},
			{
				"<leader>fg",
				function()
					builtin.grep_string({ search = vim.fn.input("Grep > ") })
				end,
				desc = "Search grep",
			},
			{ "<leader>fb", builtin.buffers,       desc = "Search bufers" },
			{ "<leader>fh", builtin.help_tags,     desc = "Search help" },
			{ "<leader>u",  ":Telescope undo<CR>", desc = "Open Telescope Undo" },
		})

		require("telescope").load_extension("undo")
	end,
}
