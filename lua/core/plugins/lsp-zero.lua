return {
	"VonHeikemen/lsp-zero.nvim",
	branch = "v3.x",
	dependencies = {
		"neovim/nvim-lspconfig",
		"stevearc/conform.nvim",
		"williamboman/mason.nvim",
		"williamboman/mason-lspconfig.nvim",
	},
	config = function()
		local lspZero = require("lsp-zero")
		local wk = require("which-key")
		local fmt = require("conform")

		lspZero.on_attach(function(_, buffer)
			wk.add({
				{ "<leader>rs", ":LspRestart<CR>",                        desc = "Restart LSP" },
				{ "K",          ":lua vim.lsp.buf.hover()<CR>",           desc = "Hover" },
				{ "gd",         ":lua vim.lsp.buf.definition()<CR>",      desc = "Go to definition" },
				{ "gD",         ":lua vim.lsp.buf.declaration()<CR>",     desc = "Go to declaration" },
				{ "gi",         ":lua vim.lsp.buf.implementation()",      desc = "Go to implementation" },
				{ "go",         ":lua vim.lsp.buf.type_definition()<CR>", desc = "Go to type definition" },
				{ "gr",         ":lua vim.lsp.buf.references()<CR>",      desc = "Go to references" },
				{ "gs",         ":lua vim.lsp.buf.signature_help()<CR>",  desc = "Go to signature" },
				{ "gl",         ":lua vim.diagnostic.open_float()<CR>",   desc = "Open diagnostics" },
				{ "]d",         ":lua vim.diagnostic.goto_next()<CR>",    desc = "Go to next diagnostic" },
				{ "[d",         ":lua vim.diagnostic.goto_prev()<CR>",    desc = "Go to previous diagnostic" },
				{ "<leader>ra", ":lua vim.lsp.buf.rename()<CR>",          desc = "Rename" },
				{ "<leader>ca", ":lua vim.lsp.buf.code_action()<CR>",     desc = "Code action" },
			}, { buffer = buffer })
		end)

		lspZero.set_sign_icons({
			error = "✘",
			warn = "▲",
			hint = "⚑",
			info = "»",
		})

		-- FORMATTING
		fmt.setup({
			formatters_by_ft = {
				tex = { "latexindent" },
				lua = { "stylua" },
				go = { "goimports" },
				c = { "clang-format" },
			},
			format_on_save = {
				timeout_ms = 500,
				lsp_fallback = true,
			},
			formatters = {
				golines = {
					prepend_args = {
						"-m",
						"80",
					},
				},
			},
		})

		-- LSP CONFIG
		local lsp = require("lspconfig")

		lsp.html.setup({
			cmd = { "vscode-html-language-server", "--stdio" },
			filetypes = { "html" },
			init_options = {
				configurationSection = { "html", "css", "javascript", "go" },
				embeddedLanguages = {
					css = true,
					javascript = true,
					go = true,
				},
			},
			provideFormatter = true,
			settings = {},
		})

		lsp.lua_ls.setup({
			settings = { -- custom settings for lua
				Lua = {
					-- make the language server recognize "vim" global
					diagnostics = {
						globals = { "vim" },
					},
					workspace = {
						-- make language server aware of runtime files
						library = {
							[vim.fn.expand("$VIMRUNTIME/lua")] = true,
							[vim.fn.stdpath("config") .. "/lua"] = true,
						},
					},
				},
			},
		})

		lsp.ltex.setup({
			filetypes = { "tex" },
		})

		lsp.ts_ls.setup({
			settings = {
				implicitProjectConfiguration = {
					checkJS = true,
				},
			},
		})

		require("mason").setup({})
		require("mason-lspconfig").setup({
			ensure_installed = {},
			handlers = {
				lspZero.default_setup,
			},
		})
	end,
}
