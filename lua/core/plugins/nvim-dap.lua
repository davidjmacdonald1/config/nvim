return {
	"rcarriga/nvim-dap-ui",
	dependencies = {
		"mfussenegger/nvim-dap",
		"theHamsta/nvim-dap-virtual-text",
		"leoluz/nvim-dap-go",
		"nvim-neotest/nvim-nio",
	},
	config = function()
		local dap = require("dap")
		local ui = require("dapui")
		local wk = require("which-key")

		ui.setup()
		require("dap-go").setup()

		wk.add({
			{ "<leader>dp", dap.toggle_breakpoint, desc = "Toggle breakpoint" },
			{ "<leader>dc", dap.run_to_cursor, desc = "Run to cursor" },
			{
				"<leader>dk",
				function()
					ui.eval(nil)
				end,
				desc = "Show value under cursor",
			},
			{
				"<leader>dK",
				function()
					ui.eval(nil, { enter = true })
				end,
				desc = "Show value under cursor (full)",
			},
			{ "<leader>dd", dap.continue, desc = "Continue debugger" },
			{ "<leader>di", dap.step_into, desc = "Step into" },
			{ "<leader>dn", dap.step_over, desc = "Step over" },
			{ "<leader>dx", dap.step_out, desc = "Step out" },
			{ "<leader>dN", dap.step_back, desc = "Step back" },
			{ "<leader>dr", dap.restart, desc = "Restart debugger" },
		})

		vim.api.nvim_set_hl(0, "blue", { fg = "#3d59a1" })
		vim.api.nvim_set_hl(0, "red", { fg = "#ff5050" })
		vim.api.nvim_set_hl(0, "green", { fg = "#9ece6a" })
		vim.api.nvim_set_hl(0, "yellow", { fg = "#FFFF00" })
		vim.api.nvim_set_hl(0, "orange", { fg = "#f09000" })
		vim.fn.sign_define(
			"DapBreakpoint",
			{ text = "●", texthl = "red", linehl = "DapBreakpoint", numhl = "DapBreakpoint" }
		)
		vim.fn.sign_define(
			"DapBreakpointCondition",
			{ text = "●", texthl = "red", linehl = "DapBreakpoint", numhl = "DapBreakpoint" }
		)
		vim.fn.sign_define(
			"DapBreakpointRejected",
			{ text = "●", texthl = "orange", linehl = "DapBreakpoint", numhl = "DapBreakpoint" }
		)
		vim.fn.sign_define(
			"DapStopped",
			{ text = "➜", texthl = "green", linehl = "DapBreakpoint", numhl = "DapBreakpoint" }
		)
		vim.fn.sign_define(
			"DapLogPoint",
			{ text = "●", texthl = "yellow", linehl = "DapBreakpoint", numhl = "DapBreakpoint" }
		)

		dap.listeners.before.attach.dapui_config = function()
			ui.open()
		end
		dap.listeners.before.launch.dapui_config = function()
			ui.open()
		end
		dap.listeners.before.event_terminated.dapui_config = function()
			ui.close()
		end
		dap.listeners.before.event_exited.dapui_config = function()
			ui.close()
		end

		---@param dir "next"|"prev"
		local function gotoBreakpoint(dir)
			local breakpoints = require("dap.breakpoints").get()
			if #breakpoints == 0 then
				vim.notify("No breakpoints set", vim.log.levels.WARN)
				return
			end
			local points = {}
			for bufnr, buffer in pairs(breakpoints) do
				for _, point in ipairs(buffer) do
					table.insert(points, { bufnr = bufnr, line = point.line })
				end
			end

			local current = {
				bufnr = vim.api.nvim_get_current_buf(),
				line = vim.api.nvim_win_get_cursor(0)[1],
			}

			local nextPoint
			for i = 1, #points do
				local isAtBreakpointI = points[i].bufnr == current.bufnr and points[i].line == current.line
				if isAtBreakpointI then
					local nextIdx = dir == "next" and i + 1 or i - 1
					if nextIdx > #points then
						nextIdx = 1
					end
					if nextIdx == 0 then
						nextIdx = #points
					end
					nextPoint = points[nextIdx]
					break
				end
			end
			if not nextPoint then
				nextPoint = points[1]
			end

			vim.cmd(("buffer +%s %s"):format(nextPoint.line, nextPoint.bufnr))
		end

		wk.add({
			{
				"]p",
				function()
					gotoBreakpoint("next")
				end,
				desc = "Next breakpoint",
			},
			{
				"[p",
				function()
					gotoBreakpoint("prev")
				end,
				desc = "Prev breakpoint",
			},
		})
	end,
}
