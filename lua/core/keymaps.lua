local wk = require("which-key")
local fns = require("functions")

wk.add({
	mode = "n",
	{ "<Esc>",         ":nohlsearch<CR>",    desc = "Clear search" },
	{ "<tab>",         ">>",                 desc = "Indent line" },
	{ "<S-tab>",       "<<",                 desc = "Dedent line" },
	{ "<A-Up>",        ":m .-2<CR>==",       desc = "Move line up" },
	{ "<A-Down>",      ":m .+1<CR>==",       desc = "Move line down" },
	{ "<C-Left>",      "<C-w>h",             desc = "Window left" },
	{ "<C-Right>",     "<C-w>l",             desc = "Window right" },
	{ "<C-Down>",      "<C-w>j",             desc = "Window down" },
	{ "<C-Up>",        "<C-w>k",             desc = "Window up" },
	{ "<leader>x",     ":!chmod +x %<CR>",   desc = "Make file executable" },
	{ "<leader><S-v>", "<C-v>",              desc = "Visual block mode" },
	{ "<leader>w",     ":WhichKey<CR>",      desc = "Open WhichKey" },
	{ "<leader>tt",    fns.toggle_test_file, desc = "Toggle test file" },
})

wk.add({
	silent = false,
	{ "<leader>s", ":%s/<C-r><C-w>/<C-r><C-w>/gI<Left><Left><Left>", desc = "Replace all matches in file" },
})

wk.add({
	mode = "v",
	{ "<tab>",    ">",                desc = "Indent selection" },
	{ "<S-tab>",  "<",                desc = "Dedent selection" },
	{ "<A-Up>",   ":m '<-2<CR>gv=gv", desc = "Move selection up" },
	{ "<A-Down>", ":m '>+1<CR>gv=gv", desc = "Move selection down" },
})

wk.add({
	mode = "v",
	silent = false,
	{ "<leader>s", ":s///g<Left><Left><Left>", desc = "Replace all matches in selection" },
})

wk.add({
	mode = "i",
	{ "<A-Up>",   "<C-o>:m .-2<CR><C-o>==", desc = "Move line up" },
	{ "<A-Down>", "<C-o>:m .+1<CR><C-o>==", desc = "Move line down" },
})
