vim.api.nvim_create_autocmd("VimEnter", {
	desc = "Start vim in the argument directory",
	callback = function()
		local file = vim.v.argv[3]
		if file == nil then
			return
		end

		local dirs = vim.fn.fnamemodify(file, ":p")
		if vim.fn.fnamemodify(dirs, ":t") ~= "" then
			dirs = vim.fn.fnamemodify(dirs, ":h")
		end

		vim.cmd("silent !mkdir -p " .. dirs)

		local modifiers = ":p"
		while file ~= "/" do
			file = vim.fn.fnamemodify(file, modifiers)
			modifiers = modifiers .. ":h"

			if vim.fn.isdirectory(file) == 1 then
				break
			end
		end

		vim.cmd("cd " .. file)
	end
})

vim.api.nvim_create_autocmd("TextYankPost", {
	desc = "Highlight when yanking (copying) text",
	group = vim.api.nvim_create_augroup("kickstart-highlight-yank", { clear = true }),
	callback = function()
		vim.highlight.on_yank()
	end,
})

vim.api.nvim_create_autocmd("FileType", {
	pattern = "*",
	desc = "Set tabs to 2 for specific file types",
	group = vim.api.nvim_create_augroup("tab-two", { clear = true }),
	callback = function()
		local types = { "dart", "yaml" }
		if vim.tbl_contains(types, vim.bo.filetype) then
			vim.opt.tabstop = 2
			vim.opt.softtabstop = 2
			vim.opt.shiftwidth = 2
		else
			vim.opt.tabstop = 4
			vim.opt.softtabstop = 4
			vim.opt.shiftwidth = 4
		end
	end,
})

vim.api.nvim_create_autocmd("BufWritePre", {
	desc = "Remove all trailing whitespace on file write",
	group = vim.api.nvim_create_augroup("trim-on-write", { clear = true }),
	callback = function()
		local l = vim.fn.line(".")
		local c = vim.fn.col(".")
		vim.cmd("%s/\\s\\+$//e")
		vim.fn.cursor(l, c)
	end
})

vim.cmd("autocmd BufWritePre * retab")
