local function toggle_go_style_test_file(file_name)
	local name, ext = string.match(file_name, "(.+)%.(.+)")
	if name:sub(-5) == "_test" then
		name = name:sub(1, -6)
	else
		name = name .. "_test"
	end

	if name and ext then
		file_name = name .. "." .. ext
	else
		file_name = name
	end

	vim.api.nvim_command("edit " .. file_name)
end

return function()
	local file_name = vim.api.nvim_buf_get_name(0)
	if file_name == "" then
		print("No current file")
		return
	end

	local file_type = vim.bo.filetype
	if file_type == "odin" or file_type == "go" then
		toggle_go_style_test_file(file_name)
	else
		print("Test file toggle is unsupported for file type " .. file_type)
	end
end
