vim.g.mapleader = " "

vim.filetype.add({
	extension = {
		templ = "templ",
	},
})

require("options")
require("auto_commands")
require("core.lazy")
require("core.keymaps")
